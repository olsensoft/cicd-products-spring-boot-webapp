package mypackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
@CrossOrigin
public class CiCdEmployeesController {

    @Autowired
    private EmployeeRepository repository;

    @GetMapping(value="/employees", produces={"application/json","application/xml"})
    public ResponseEntity<Collection<Employee>> getEmployees() {
        return ResponseEntity
                .ok()
                .body(repository.getEmployees());
    }
}
