def projectName = 'cicd-products'
def version = "0.0.${currentBuild.number}"

def appName = "${projectName}-app"
def mySqlName = "${projectName}-mysql"

def dockerAppImageTag = "${projectName}-app:${version}"
def dockerMySqlImageTag = "${projectName}-mysql:${version}"

pipeline {
    agent any

    stages {

        // If you want to build your JAR file as part of the Jenkins build:
        /*
        stage('Package Spring Boot application into a JAR via Maven') {
            steps {
                sh "mvn package"
            }
        }
        */

        stage('Build docker images') {
            steps {
                sh "docker build -f Dockerfile-app -t ${dockerAppImageTag} ."
                sh "docker build -f Dockerfile-mysql -t ${dockerMySqlImageTag} ."
            }
        }

        stage('Deploy images To OpenShift') {
            steps {
                sh "oc login https://localhost:8443 --username admin --password admin --insecure-skip-tls-verify=true"

                sh "oc project ${projectName} || oc new-project ${projectName}"

                sh "oc delete all -l app=${appName} || echo 'Unable to delete all previous OpenShift app resources'"
                sh "oc delete all -l app=${mySqlName} || echo 'Unable to delete all previous OpenShift MySql resources'"

                sh "oc new-app ${appName}:${version}   -l version=${version}"
                sh "oc new-app ${mySqlName}:${version} -l version=${version}"

                sh "oc expose svc/${appName}"
            }
        }
    }
}
